﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesAjedrez
{
    public class Sala
    {
        private int _idsala;
        private int _capacidad;
        private string _medios;
        private string _hotel_nombre;

        public int Idsala { get => _idsala; set => _idsala = value; }
        public int Capacidad { get => _capacidad; set => _capacidad = value; }
        public string Medios { get => _medios; set => _medios = value; }
        public string Hotel_nombre { get => _hotel_nombre; set => _hotel_nombre = value; }
    }
}
