﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesAjedrez
{
    public class Árbitro
    {
        private int _idarbitro;
        private int _campjugados;
        private string _participante_numasociado;

        public int Idarbitro { get => _idarbitro; set => _idarbitro = value; }
        public int Campjugados { get => _campjugados; set => _campjugados = value; }
        public string Participante_numasociado { get => _participante_numasociado; set => _participante_numasociado = value; }
    }
}
