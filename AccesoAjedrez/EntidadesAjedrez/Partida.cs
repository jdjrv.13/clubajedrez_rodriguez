﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesAjedrez
{
    public class Partida
    {
        private string _codp;
        private int _arbitro_idarbitro;
        private int _jornada_idjornada;
        private int _sala_idsala;
        private int _entradas;

        public string Codp { get => _codp; set => _codp = value; }
        public int Arbitro_idarbitro { get => _arbitro_idarbitro; set => _arbitro_idarbitro = value; }
        public int Jornada_idjornada { get => _jornada_idjornada; set => _jornada_idjornada = value; }
        public int Sala_idsala { get => _sala_idsala; set => _sala_idsala = value; }
        public int Entradas { get => _entradas; set => _entradas = value; }
    }
}
