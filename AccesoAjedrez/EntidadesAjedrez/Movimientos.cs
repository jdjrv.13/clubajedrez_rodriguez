﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesAjedrez
{
    public class Movimientos
    {
        private string _partidas_codp;
        private int _idmovimiento;
        private string _movimiento;
        private string _comentario;

        public string Partidas_codp { get => _partidas_codp; set => _partidas_codp = value; }
        public int Idmovimiento { get => _idmovimiento; set => _idmovimiento = value; }
        public string Movimiento { get => _movimiento; set => _movimiento = value; }
        public string Comentario { get => _comentario; set => _comentario = value; }
    }
}
