﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesAjedrez
{
    public class Color1
    {
        private int _idcolor;
        private int _jugador_idjugador;
        private string _color;
        private string _partidas_codp;

        public int Idcolor { get => _idcolor; set => _idcolor = value; }
        public int Jugador_idjugador { get => _jugador_idjugador; set => _jugador_idjugador = value; }
        public string Color { get => _color; set => _color = value; }
        public string Partidas_codp { get => _partidas_codp; set => _partidas_codp = value; }
    }
}
