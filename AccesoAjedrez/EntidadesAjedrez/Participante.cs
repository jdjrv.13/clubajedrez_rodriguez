﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesAjedrez
{
    public class Participante
    {
        private string _numasociado;
        private string _nombre;
        private string _direccion;
        private string _telefono;
        private string _pais_numcorrid;

        public string Numasociado { get => _numasociado; set => _numasociado = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
        public string Direccion { get => _direccion; set => _direccion = value; }
        public string Telefono { get => _telefono; set => _telefono = value; }
        public string Pais_numcorrid { get => _pais_numcorrid; set => _pais_numcorrid = value; }
    }
}
