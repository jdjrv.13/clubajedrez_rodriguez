﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesAjedrez
{
    public class Jornada
    {
        private int _idjornada;
        private string _perido;
        private string _año;

        public int Idjornada { get => _idjornada; set => _idjornada = value; }
        public string Perido { get => _perido; set => _perido = value; }
        public string Año { get => _año; set => _año = value; }
    }
}
