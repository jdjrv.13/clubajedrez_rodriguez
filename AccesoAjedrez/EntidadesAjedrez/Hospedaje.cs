﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesAjedrez
{
    public class Hospedaje
    {
        private int _idhospedaje;
        private string _participante_numasociado;
        private string _hotel_nombre;
        private string _fechaentrada;
        private string _fechasalida;

        public int Idhospedaje { get => _idhospedaje; set => _idhospedaje = value; }
        public string Participante_numasociado { get => _participante_numasociado; set => _participante_numasociado = value; }
        public string Hotel_nombre { get => _hotel_nombre; set => _hotel_nombre = value; }
        public string Fechaentrada { get => _fechaentrada; set => _fechaentrada = value; }
        public string Fechasalida { get => _fechasalida; set => _fechasalida = value; }
    }
}
