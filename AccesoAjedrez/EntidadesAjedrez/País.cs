﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesAjedrez
{
    public class País
    {
        private string _numcorrid;
        private string _nombre;
        private int _numclubes;
        private string _representa;

        public string Numcorrid { get => _numcorrid; set => _numcorrid = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
        public int Numclubes { get => _numclubes; set => _numclubes = value; }
        public string Representa { get => _representa; set => _representa = value; }
    }
}
