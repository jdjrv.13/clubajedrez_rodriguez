﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesAjedrez
{
    public class Jugador
    {
        private int _idjugador;
        private int _nivel;
        private int _campjugados;
        private string _participante_numasociado;

        public int Idjugador { get => _idjugador; set => _idjugador = value; }
        public int Nivel { get => _nivel; set => _nivel = value; }
        public int Campjugados { get => _campjugados; set => _campjugados = value; }
        public string Participante_numasociado { get => _participante_numasociado; set => _participante_numasociado = value; }
    }
}
