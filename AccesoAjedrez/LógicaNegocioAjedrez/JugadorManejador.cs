﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoAjedrez;
using EntidadesAjedrez;

namespace LógicaNegocioAjedrez
{
    public class JugadorManejador
    {
        JugadorAccesoDatos _jugadorAccesoDatos = new JugadorAccesoDatos();

        public Tuple<bool, string> ValidarJugador(Jugador jugador)
        {
            bool error = true;
            string cadenaErrores = "";

            if (jugador.Participante_numasociado.Length == 0 || jugador.Participante_numasociado == null)
            {
                cadenaErrores = cadenaErrores + "* El campo número de asociado no puede ser vacío \n";
                error = false;
            }

            if (jugador.Participante_numasociado.Length != 6)
            {
                cadenaErrores = cadenaErrores + "* Sólo se permiten 6 caracteres para el número asociado \n";
                error = false;
            }

            var valida = new Tuple<bool, string>(error, cadenaErrores);
            return valida;
        }

        public void GuardarJugador(Jugador jugador)
        {
            try
            {
                _jugadorAccesoDatos.GuardarJugador(jugador);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void ActualizarJugador(Jugador jugador)
        {
            try
            {
                _jugadorAccesoDatos.ActualizarJugador(jugador);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void EliminarJugador(string jugador)
        {
            try
            {
                _jugadorAccesoDatos.EliminarJugador(jugador);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        public List<Jugador> ObtenerJugador(string consulta)
        {
            var listaJugador = _jugadorAccesoDatos.ObtenerJugador(consulta);

            return listaJugador;
        }
    }
}
