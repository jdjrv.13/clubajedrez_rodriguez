﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoAjedrez;
using EntidadesAjedrez;

namespace LógicaNegocioAjedrez
{
    public class SalaManejador
    {
        SalaAccesoDatos _salaAccesoDatos = new SalaAccesoDatos();

        public Tuple<bool, string> ValidarSala(Sala sala)
        {
            bool error = true;
            string cadenaErrores = "";

            if (sala.Hotel_nombre.Length == 0 || sala.Hotel_nombre == null)
            {
                cadenaErrores = cadenaErrores + "* El campo nombre de hotel no puede ser vacío \n";
                error = false;
            }

            var valida = new Tuple<bool, string>(error, cadenaErrores);
            return valida;
        }

        public void GuardarSala(Sala sala)
        {
            try
            {
                _salaAccesoDatos.GuardarSala(sala);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void ActualizarSala(Sala sala)
        {
            try
            {
                _salaAccesoDatos.ActualizarSala(sala);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void EliminarSala(string sala)
        {
            try
            {
                _salaAccesoDatos.EliminarSala(sala);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        public List<Sala> ObtenerSala(string consulta)
        {
            var listaSala = _salaAccesoDatos.ObtenerSala(consulta);

            return listaSala;
        }
    }
}
