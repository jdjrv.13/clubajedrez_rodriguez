﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntidadesAjedrez;
using AccesoAjedrez;

namespace LógicaNegocioAjedrez
{
    public class HospedajeManejador
    {
        HospedajeAccesoDatos _hospedajeAccesoDatos = new HospedajeAccesoDatos();

        public Tuple<bool, string> ValidarHospedaje(Hospedaje hospedaje)
        {
            bool error = true;
            string cadenaErrores = "";

            if (hospedaje.Participante_numasociado.Length != 6 || hospedaje.Participante_numasociado == null)
            {
                cadenaErrores = cadenaErrores + "* El campo número de asociado debe ser de 6 caracteres \n";
                error = false;
            }

            if (hospedaje.Hotel_nombre.Length == 0 || hospedaje.Hotel_nombre == null)
            {
                cadenaErrores = cadenaErrores + "* El campo nombre de hotel no debe ser vacío \n";
                error = false;
            }

            var valida = new Tuple<bool, string>(error, cadenaErrores);
            return valida;
        }

        public void GuardarHospedaje(Hospedaje hospedaje)
        {
            try
            {
                _hospedajeAccesoDatos.GuardarHospedaje(hospedaje);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void ActualizarHospedaje(Hospedaje hospedaje)
        {
            try
            {
                _hospedajeAccesoDatos.ActualizarHospedaje(hospedaje);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void EliminarHospedaje(string hospedaje)
        {
            try
            {
                _hospedajeAccesoDatos.EliminarHospedaje(hospedaje);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        public List<Hospedaje> ObtenerHospedaje(string consulta)
        {
            var listaHospedaje = _hospedajeAccesoDatos.ObtenerHospedaje(consulta);

            return listaHospedaje;
        }
    }
}
