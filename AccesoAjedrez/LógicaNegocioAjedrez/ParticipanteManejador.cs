﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoAjedrez;
using EntidadesAjedrez;

namespace LógicaNegocioAjedrez
{
    public class ParticipanteManejador
    {
        ParticipanteAccesoDatos _participanteAccesoDatos = new ParticipanteAccesoDatos();

        public Tuple<bool, string> ValidarParticipante(Participante participante)
        {
            bool error = true;
            string cadenaErrores = "";

            if (participante.Numasociado.Length == 0 || participante.Numasociado == null)
            {
                cadenaErrores = cadenaErrores + "* El campo número de asociado no puede ser vacío \n";
                error = false;
            }

            if (participante.Numasociado.Length != 6)
            {
                cadenaErrores = cadenaErrores + "* Sólo se permiten 6 caracteres para el número de asociado \n";
                error = false;
            }

            if (participante.Pais_numcorrid.Length == 0 || participante.Pais_numcorrid == null)
            {
                cadenaErrores = cadenaErrores + "* El campo número correlativo no puede ser vacío \n";
                error = false;
            }

            var valida = new Tuple<bool, string>(error, cadenaErrores);
            return valida;
        }

        public void GuardarParticipante(Participante participante)
        {
            try
            {
                _participanteAccesoDatos.GuardarParticipante(participante);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void ActualizarParticipante(Participante participante)
        {
            try
            {
                _participanteAccesoDatos.ActualizarParticipante(participante);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void EliminarParticipante(string participante)
        {
            try
            {
                _participanteAccesoDatos.EliminarParticipante(participante);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        public List<Participante> ObtenerParticipante(string consulta)
        {
            var listaParticipante = _participanteAccesoDatos.ObtenerParticipante(consulta);

            return listaParticipante;
        }
    }
}
