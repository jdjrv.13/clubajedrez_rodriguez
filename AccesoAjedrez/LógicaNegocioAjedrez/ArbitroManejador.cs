﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoAjedrez;
using EntidadesAjedrez;

namespace LógicaNegocioAjedrez
{
    public class ArbitroManejador
    {
        ÁrbitroAccesoDatos _arbitroAccesoDatos = new ÁrbitroAccesoDatos();

        public Tuple<bool, string> ValidarArbitro(Árbitro arbitro)
        {
            bool error = true;
            string cadenaErrores = "";

            if (arbitro.Participante_numasociado.Length == 0 || arbitro.Participante_numasociado == null)
            {
                cadenaErrores = cadenaErrores + "* El campo número de asociado no puede ser vacío \n";
                error = false;
            }

            if (arbitro.Participante_numasociado.Length != 6)
            {
                cadenaErrores = cadenaErrores + "* Sólo se permiten 6 caracteres para el número asociado \n";
                error = false;
            }

            var valida = new Tuple<bool, string>(error, cadenaErrores);
            return valida;
        }

        public void GuardarArbitro(Árbitro arbitro)
        {
            try
            {
                _arbitroAccesoDatos.GuardarÁrbitro(arbitro);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void ActualizarArbitro(Árbitro arbitro)
        {
            try
            {
                _arbitroAccesoDatos.ActualizarÁrbitro(arbitro);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void EliminarArbitro(string arbitro)
        {
            try
            {
                _arbitroAccesoDatos.EliminarÁrbitro(arbitro);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        public List<Árbitro> ObtenerArbitro(string consulta)
        {
            var listaArbitro = _arbitroAccesoDatos.ObtenerÁrbitro(consulta);

            return listaArbitro;
        }
    }
}
