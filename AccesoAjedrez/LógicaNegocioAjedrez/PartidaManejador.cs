﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoAjedrez;
using EntidadesAjedrez;

namespace LógicaNegocioAjedrez
{
    public class PartidaManejador
    {
        PartidaAccesoDatos _partidaAccesoDatos = new PartidaAccesoDatos();

        public Tuple<bool, string> ValidarPartida(Partida partida)
        {
            bool error = true;
            string cadenaErrores = "";

            if (partida.Codp.Length == 0 || partida.Codp == null)
            {
                cadenaErrores = cadenaErrores + "* El campo código de partida no puede ser vacío \n";
                error = false;
            }

            if (partida.Codp.Length != 5)
            {
                cadenaErrores = cadenaErrores + "* Sólo se permiten 5 caracteres para el código de partida \n";
                error = false;
            }

            if (partida.Arbitro_idarbitro < 0)
            {
                cadenaErrores = cadenaErrores + "* Es necesario usar el número relacionado idarbitro, no puede ser vacío \n";
                error = false;
            }

            if (partida.Jornada_idjornada < 0)
            {
                cadenaErrores = cadenaErrores + "* Es necesario usar el número relacionado idjornada, no puede ser vacío \n";
                error = false;
            }

            if (partida.Sala_idsala < 0)
            {
                cadenaErrores = cadenaErrores + "* Es necesario usar el número relacionado idsala, no puede ser vacío \n";
                error = false;
            }

            var valida = new Tuple<bool, string>(error, cadenaErrores);
            return valida;
        }

        public void GuardarPartida(Partida partida)
        {
            try
            {
                _partidaAccesoDatos.GuardarPartida(partida);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void ActualizarPartida(Partida partida)
        {
            try
            {
                _partidaAccesoDatos.ActualizarPartida(partida);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void EliminarPartida(string partida)
        {
            try
            {
                _partidaAccesoDatos.EliminarPartida(partida);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        public List<Partida> ObtenerPartida(string consulta)
        {
            var listaPartida = _partidaAccesoDatos.ObtenerPartida(consulta);

            return listaPartida;
        }
    }
}
