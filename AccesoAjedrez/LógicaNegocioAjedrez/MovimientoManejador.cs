﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoAjedrez;
using EntidadesAjedrez;

namespace LógicaNegocioAjedrez
{
    public class MovimientoManejador
    {
        MovimientoAccesoDatos _movimientoAccesoDatos = new MovimientoAccesoDatos();

        public Tuple<bool, string> ValidarMovimiento(Movimientos movimiento)
        {
            bool error = true;
            string cadenaErrores = "";

            if (movimiento.Partidas_codp.Length == 0 || movimiento.Partidas_codp == null)
            {
                cadenaErrores = cadenaErrores + "* El campo código de partida no puede ser vacío \n";
                error = false;
            }

            if (movimiento.Partidas_codp.Length != 5)
            {
                cadenaErrores = cadenaErrores + "* Sólo se permiten 5 caracteres para el código de partida \n";
                error = false;
            }

            var valida = new Tuple<bool, string>(error, cadenaErrores);
            return valida;
        }

        public void GuardarMovimiento(Movimientos movimiento)
        {
            try
            {
                _movimientoAccesoDatos.GuardarMovimiento(movimiento);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void ActualizarMovimiento(Movimientos movimiento)
        {
            try
            {
                _movimientoAccesoDatos.ActualizarMovimiento(movimiento);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void EliminarMovimiento(string movimiento)
        {
            try
            {
                _movimientoAccesoDatos.EliminarMovimiento(movimiento);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        public List<Movimientos> ObtenerMovimiento(string consulta)
        {
            var listaMovimiento = _movimientoAccesoDatos.ObtenerMovimiento(consulta);

            return listaMovimiento;
        }
    }
}
