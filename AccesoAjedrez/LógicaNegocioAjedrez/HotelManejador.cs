﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoAjedrez;
using EntidadesAjedrez;

namespace LógicaNegocioAjedrez
{
    public class HotelManejador
    {
        HotelAccesoDatos _hotelAccesoDatos = new HotelAccesoDatos();

        public Tuple<bool, string> ValidarHotel(Hotel hotel)
        {
            bool error = true;
            string cadenaErrores = "";

            if (hotel.Nombre.Length == 0 || hotel.Nombre == null)
            {
                cadenaErrores = cadenaErrores + "* El campo nombre no puede ser vacío \n";
                error = false;
            }

            var valida = new Tuple<bool, string>(error, cadenaErrores);
            return valida;
        }

        public void GuardarHotel(Hotel hotel)
        {
            try
            {
                _hotelAccesoDatos.GuardarHotel(hotel);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void ActualizarHotel(Hotel hotel)
        {
            try
            {
                _hotelAccesoDatos.ActualizarHotel(hotel);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void EliminarHotel(string hotel)
        {
            try
            {
                _hotelAccesoDatos.EliminarHotel(hotel);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        public List<Hotel> ObtenerHotel(string consulta)
        {
            var listaHotel = _hotelAccesoDatos.ObtenerHotel(consulta);

            return listaHotel;
        }
    }
}
