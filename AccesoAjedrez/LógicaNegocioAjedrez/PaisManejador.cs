﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoAjedrez;
using EntidadesAjedrez;

namespace LógicaNegocioAjedrez
{
    public class PaisManejador
    {
        PaísAccesoDatos _paísAccesoDatos = new PaísAccesoDatos();

        public Tuple<bool, string> ValidarPais(País pais)
        {
            bool error = true;
            string cadenaErrores = "";

            if (pais.Numcorrid.Length == 0 || pais.Numcorrid == null)
            {
                cadenaErrores = cadenaErrores + "* El campo número correlativo no puede ser vacío \n";
                error = false;
            }

            if (pais.Numcorrid.Length != 4)
            {
                cadenaErrores = cadenaErrores + "* Sólo se permiten 4 caracteres para el número correlativo \n";
                error = false;
            }

            var valida = new Tuple<bool, string>(error, cadenaErrores);
            return valida;
        }

        public void GuardarPais(País pais)
        {
            try
            {
                _paísAccesoDatos.GuardarPais(pais);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void ActualizarPais(País pais)
        {
            try
            {
                _paísAccesoDatos.ActualizarPais(pais);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void EliminarPais(string pais)
        {
            try
            {
                _paísAccesoDatos.EliminarPais(pais);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        public List<País> ObtenerPais(string consulta)
        {
            var listaUsuarios = _paísAccesoDatos.ObtenerPais(consulta);

            return listaUsuarios;
        }
    }
}
