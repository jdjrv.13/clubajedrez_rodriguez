﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoAjedrez;
using EntidadesAjedrez;

namespace LógicaNegocioAjedrez
{
    public class JornadaManejador
    {
        JornadaAccesoDatos _jornadaAccesoDatos = new JornadaAccesoDatos();

        public Tuple<bool, string> ValidarJornada(Jornada jornada)
        {
            bool error = true;
            string cadenaErrores = "";

            if (jornada.Perido.Length == 0 || jornada.Perido == null)
            {
                cadenaErrores = cadenaErrores + "* El campo periodo no puede ser vacío \n";
                error = false;
            }

            if (jornada.Año.Length == 0 || jornada.Año == null)
            {
                cadenaErrores = cadenaErrores + "* El capo año no puede ser vacío \n";
                error = false;
            }

            var valida = new Tuple<bool, string>(error, cadenaErrores);
            return valida;
        }

        public void GuardarJornada(Jornada jornada)
        {
            try
            {
                _jornadaAccesoDatos.GuardarJornada(jornada);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void ActualizarJornada(Jornada jornada)
        {
            try
            {
                _jornadaAccesoDatos.ActualizarJornada(jornada);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void EliminarJornada(string jornada)
        {
            try
            {
                _jornadaAccesoDatos.EliminarJornada(jornada);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        public List<Jornada> ObtenerJornada(string consulta)
        {
            var listaJornada = _jornadaAccesoDatos.ObtenerJornada(consulta);

            return listaJornada;
        }
    }
}
