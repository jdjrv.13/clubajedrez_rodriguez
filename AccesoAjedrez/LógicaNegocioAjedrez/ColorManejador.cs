﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntidadesAjedrez;
using AccesoAjedrez;

namespace LógicaNegocioAjedrez
{
    public class ColorManejador
    {
        ColorAccesoDatos _colorAccesoDatos = new ColorAccesoDatos();

        public Tuple<bool, string> ValidarColor(Color1 color)
        {
            bool error = true;
            string cadenaErrores = "";

            if (color.Jugador_idjugador < 0)
            {
                cadenaErrores = cadenaErrores + "* El campo debe ser mayor a 0 no puede ser vacío \n";
                error = false;
            }

            if (color.Partidas_codp.Length != 5)
            {
                cadenaErrores = cadenaErrores + "* Sólo se permiten 5 caracteres para código de partida \n";
                error = false;
            }

            var valida = new Tuple<bool, string>(error, cadenaErrores);
            return valida;
        }

        public void GuardarColor(Color1 color)
        {
            try
            {
                _colorAccesoDatos.GuardarColor(color);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void ActualizarColor(Color1 color)
        {
            try
            {
                _colorAccesoDatos.ActualizarColor(color);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void EliminarColor(string color)
        {
            try
            {
                _colorAccesoDatos.EliminarColor(color);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        public List<Color1> ObtenerColor(string consulta)
        {
            var listaColor = _colorAccesoDatos.ObtenerColor(consulta);

            return listaColor;
        }
    }
}
