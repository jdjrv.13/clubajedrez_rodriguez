﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntidadesAjedrez;

namespace AccesoAjedrez
{
    public class PartidaAccesoDatos
    {
        ConexiónBD _conexion;

        public PartidaAccesoDatos()
        {
            try
            {
                _conexion = new ConexiónBD("localhost", "root", "", "clubajedrez", 3307);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la conexión " + ex.Message);
            }
        }

        public void GuardarPartida(Partida partida)
        {
            try
            {
                string consulta = string.Format("insert into partidas values('{0}', {1}, {2}, {3}, {4})", partida.Codp, partida.Arbitro_idarbitro, partida.Jornada_idjornada, partida.Sala_idsala, partida.Entradas);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló el guardado " + ex.Message);
            }
        }

        public void ActualizarPartida(Partida partida)
        {
            try
            {
                string consulta = string.Format("update partidas set arbitro_idarbitro = {0}, jornada_idjornada = {1}, sala_idsala = {2}, entradas = {3} where codp = '{4}'", partida.Arbitro_idarbitro, partida.Jornada_idjornada, partida.Sala_idsala, partida.Entradas, partida.Codp);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la actualización " + ex.Message);
            }
        }

        public void EliminarPartida(string partida)
        {
            try
            {
                string consulta = string.Format("delete from partidas where codp = {0}", partida);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la eliminación " + ex.Message);
            }
        }

        public List<Partida> ObtenerPartida(string filtro)
        {
            var listaPartida = new List<Partida>();
            var ds = new DataSet();
            string consulta = string.Format("select * from partidas where codp like '%{0}%'", filtro);
            ds = _conexion.ObtenerDatos(consulta, "partidas");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var partida = new Partida
                {
                    Codp = row["codp"].ToString(),
                    Arbitro_idarbitro = int.Parse(row["arbitro_idarbitro"].ToString()),
                    Jornada_idjornada = int.Parse(row["jornada_idjornada"].ToString()),
                    Sala_idsala = int.Parse(row["sala_idsala"].ToString()),
                    Entradas = int.Parse(row["entradas"].ToString())
                };
                listaPartida.Add(partida);
            }
            return listaPartida;
        }
    }
}
