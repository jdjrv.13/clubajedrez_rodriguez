﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntidadesAjedrez;

namespace AccesoAjedrez
{
    public class PaísAccesoDatos
    {
        ConexiónBD _conexion;

        public PaísAccesoDatos()
        {
            try
            {
                _conexion = new ConexiónBD("localhost", "root", "", "clubajedrez", 3307);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la conexión " + ex.Message);
            }
        }

        public void GuardarPais(País pais)
        {
            try
            {
                string consulta = string.Format("insert into pais values('{0}', '{1}', {2}, '{3}')", pais.Numcorrid, pais.Nombre, pais.Numclubes, pais.Representa);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló el guardado " + ex.Message);
            }
        }

        public void ActualizarPais(País pais)
        {
            try
            {
                string consulta = string.Format("update pais set nombre = '{0}', numclubes = {1}, representa = '{2}' where numcorrid = '{3}'", pais.Nombre, pais.Numclubes, pais.Representa, pais.Numcorrid);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la actualización " + ex.Message);
            }
        }

        public void EliminarPais(string pais)
        {
            try
            {
                string consulta = string.Format("delete from pais where numcorrid = '{0}'", pais);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la eliminación " + ex.Message);
            }
        }

        public List<País> ObtenerPais(string filtro)
        {
            var listaPais = new List<País>();
            var ds = new DataSet();
            string consulta = string.Format("select * from pais where numcorrid like '%{0}%'", filtro);
            ds = _conexion.ObtenerDatos(consulta, "pais");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var pais = new País
                {
                    Numcorrid = row["numcorrid"].ToString(),
                    Nombre = row["nombre"].ToString(),
                    Numclubes = int.Parse(row["numclubes"].ToString()),
                    Representa = row["representa"].ToString()
                };
                listaPais.Add(pais);
            }
            return listaPais;
        }
    }
}
