﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntidadesAjedrez;

namespace AccesoAjedrez
{
    public class JornadaAccesoDatos
    {
        ConexiónBD _conexion;

        public JornadaAccesoDatos()
        {
            try
            {
                _conexion = new ConexiónBD("localhost", "root", "", "clubajedrez", 3307);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la conexión " + ex.Message);
            }
        }

        public void GuardarJornada(Jornada jornada)
        {
            try
            {
                string consulta = string.Format("insert into jornada values(null, '{0}', '{1}')", jornada.Perido, jornada.Año);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló el guardado " + ex.Message);
            }
        }

        public void ActualizarJornada(Jornada jornada)
        {
            try
            {
                string consulta = string.Format("update jornada set perido = '{0}', año = '{1}' where idjornada = {2}", jornada.Perido, jornada.Año, jornada.Idjornada);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la actualización " + ex.Message);
            }
        }

        public void EliminarJornada(string jornada)
        {
            try
            {
                string consulta = string.Format("delete from jornada where idjornada = {0}", jornada);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la eliminación " + ex.Message);
            }
        }

        public List<Jornada> ObtenerJornada(string filtro)
        {
            var listaJornada = new List<Jornada>();
            var ds = new DataSet();
            string consulta = string.Format("select * from jornada where idjornada like '%{0}%'", filtro);
            ds = _conexion.ObtenerDatos(consulta, "jornada");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var jornada = new Jornada
                {
                    Idjornada = int.Parse(row["idjornada"].ToString()),
                    Perido = row["perido"].ToString(),
                    Año = row["año"].ToString()
                };
                listaJornada.Add(jornada);
            }
            return listaJornada;
        }
    }
}
