﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntidadesAjedrez;

namespace AccesoAjedrez
{
    public class ÁrbitroAccesoDatos
    {
        ConexiónBD _conexion;

        public ÁrbitroAccesoDatos()
        {
            try
            {
                _conexion = new ConexiónBD("localhost", "root", "", "clubajedrez", 3307);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la conexión " + ex.Message);
            }
        }

        public void GuardarÁrbitro(Árbitro arbitro)
        {
            try
            {
                string consulta = string.Format("insert into arbitro values(null, {0}, '{1}')", arbitro.Campjugados, arbitro.Participante_numasociado);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló el guardado " + ex.Message);
            }
        }

        public void ActualizarÁrbitro(Árbitro arbitro)
        {
            try
            {
                string consulta = string.Format("update arbitro set campjugados = {0}, participante_numasociado = '{1}' where idarbitro = {2}", arbitro.Campjugados, arbitro.Participante_numasociado, arbitro.Idarbitro);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la actualización " + ex.Message);
            }
        }

        public void EliminarÁrbitro(string arbitro)
        {
            try
            {
                string consulta = string.Format("delete from arbitro where idarbitro = {0}", arbitro);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la eliminación " + ex.Message);
            }
        }

        public List<Árbitro> ObtenerÁrbitro(string filtro)
        {
            var listaArbitro = new List<Árbitro>();
            var ds = new DataSet();
            string consulta = string.Format("select * from arbitro where participante_numasociado like '%{0}%'", filtro);
            ds = _conexion.ObtenerDatos(consulta, "arbitro");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var arbitro = new Árbitro
                {
                    Idarbitro = int.Parse(row["idarbitro"].ToString()),
                    Campjugados = int.Parse(row["campjugados"].ToString()),
                    Participante_numasociado = row["participante_numasociado"].ToString()
                };
                listaArbitro.Add(arbitro);
            }
            return listaArbitro;
        }
    }
}
