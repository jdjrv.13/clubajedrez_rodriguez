﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntidadesAjedrez;

namespace AccesoAjedrez
{
    public class ParticipanteAccesoDatos
    {
        ConexiónBD _conexion;

        public ParticipanteAccesoDatos()
        {
            try
            {
                _conexion = new ConexiónBD("localhost", "root", "", "clubajedrez", 3307);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la conexión " + ex.Message);
            }
        }

        public void GuardarParticipante(Participante participante)
        {
            try
            {
                string consulta = string.Format("insert into participante values('{0}', '{1}', '{2}', '{3}', '{4}')", participante.Numasociado, participante.Nombre, participante.Direccion, participante.Telefono, participante.Pais_numcorrid);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló el guardado " + ex.Message);
            }
        }

        public void ActualizarParticipante(Participante participante)
        {
            try
            {
                string consulta = string.Format("update participante set nombre = '{0}', direccion = '{1}', telefono = '{2}', pais_numcorrid = '{3}' where numasociado = '{4}'", participante.Nombre, participante.Direccion, participante.Telefono, participante.Pais_numcorrid, participante.Numasociado);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la actualización " + ex.Message);
            }
        }

        public void EliminarParticipante(string participante)
        {
            try
            {
                string consulta = string.Format("delete from participante where numasociado = '{0}'", participante);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la eliminación " + ex.Message);
            }
        }

        public List<Participante> ObtenerParticipante(string filtro)
        {
            var listaParticipante = new List<Participante>();
            var ds = new DataSet();
            string consulta = string.Format("select * from participante where numasociado like '%{0}%'", filtro);
            ds = _conexion.ObtenerDatos(consulta, "participante");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var participante = new Participante
                {
                    Numasociado = row["numasociado"].ToString(),
                    Nombre = row["nombre"].ToString(),
                    Direccion = row["direccion"].ToString(),
                    Telefono = row["telefono"].ToString(),
                    Pais_numcorrid = row["pais_numcorrid"].ToString()
                };
                listaParticipante.Add(participante);
            }
            return listaParticipante;
        }
    }
}
