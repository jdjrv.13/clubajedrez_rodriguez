﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntidadesAjedrez;

namespace AccesoAjedrez
{
    public class JugadorAccesoDatos
    {
        ConexiónBD _conexion;

        public JugadorAccesoDatos()
        {
            try
            {
                _conexion = new ConexiónBD("localhost", "root", "", "clubajedrez", 3307);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la conexión " + ex.Message);
            }
        }

        public void GuardarJugador(Jugador jugador)
        {
            try
            {
                string consulta = string.Format("insert into jugador values(null, {0}, {1}, '{2}')", jugador.Nivel, jugador.Campjugados, jugador.Participante_numasociado);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló el guardado " + ex.Message);
            }
        }

        public void ActualizarJugador(Jugador jugador)
        {
            try
            {
                string consulta = string.Format("update jugador set nivel = {0}, campjugados = {1}, participante_numasociado = '{2}' where idjugador = {3}", jugador.Nivel, jugador.Campjugados, jugador.Participante_numasociado, jugador.Idjugador);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la actualización " + ex.Message);
            }
        }

        public void EliminarJugador(string jugador)
        {
            try
            {
                string consulta = string.Format("delete from jugador where idjugador = {0}", jugador);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la eliminación " + ex.Message);
            }
        }

        public List<Jugador> ObtenerJugador(string filtro)
        {
            var listaJugador = new List<Jugador>();
            var ds = new DataSet();
            string consulta = string.Format("select * from jugador where participante_numasociado like '%{0}%'", filtro);
            ds = _conexion.ObtenerDatos(consulta, "jugador");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var jugador = new Jugador
                {
                    Idjugador = int.Parse(row["idjugador"].ToString()),
                    Nivel = int.Parse(row["nivel"].ToString()),
                    Campjugados = int.Parse(row["campjugados"].ToString()),
                    Participante_numasociado = row["participante_numasociado"].ToString()
                };
                listaJugador.Add(jugador);
            }
            return listaJugador;
        }
    }
}
