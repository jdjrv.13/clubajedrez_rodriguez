﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntidadesAjedrez;

namespace AccesoAjedrez
{
    public class ColorAccesoDatos
    {
        ConexiónBD _conexion;

        public ColorAccesoDatos()
        {
            try
            {
                _conexion = new ConexiónBD("localhost", "root", "", "clubajedrez", 3307);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la conexión " + ex.Message);
            }
        }

        public void GuardarColor(Color1 color)
        {
            try
            {
                string consulta = string.Format("insert into color values(null, {0}, '{1}', '{2}')", color.Jugador_idjugador, color.Color, color.Partidas_codp);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló el guardado " + ex.Message);
            }
        }

        public void ActualizarColor(Color1 color)
        {
            try
            {
                string consulta = string.Format("update color set jugador_idjugador = {0}, color = '{1}', partidas_codp = '{2}' where idcolor = {3}", color.Jugador_idjugador, color.Color, color.Partidas_codp, color.Idcolor);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la actualización " + ex.Message);
            }
        }

        public void EliminarColor(string color)
        {
            try
            {
                string consulta = string.Format("delete from color where idcolor = {0}", color);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la eliminación " + ex.Message);
            }
        }

        public List<Color1> ObtenerColor(string filtro)
        {
            var listaColor = new List<Color1>();
            var ds = new DataSet();
            string consulta = string.Format("select * from color where idcolor like '%{0}%'", filtro);
            ds = _conexion.ObtenerDatos(consulta, "color");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var color = new Color1
                {
                    Idcolor = int.Parse(row["idcolor"].ToString()),
                    Jugador_idjugador = int.Parse(row["jugador_idjugador"].ToString()),
                    Color = row["color"].ToString(),
                    Partidas_codp = row["partidas_codp"].ToString()
                };
                listaColor.Add(color);
            }
            return listaColor;
        }
    }
}
