﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntidadesAjedrez;

namespace AccesoAjedrez
{
    public class HospedajeAccesoDatos
    {
        ConexiónBD _conexion;

        public HospedajeAccesoDatos()
        {
            try
            {
                _conexion = new ConexiónBD("localhost", "root", "", "clubajedrez", 3307);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la conexión " + ex.Message);
            }
        }

        public void GuardarHospedaje(Hospedaje hospedaje)
        {
            try
            {
                string consulta = string.Format("insert into hospedaje values(null, '{0}', '{1}', '{2}', '{3}')", hospedaje.Participante_numasociado, hospedaje.Hotel_nombre, hospedaje.Fechaentrada, hospedaje.Fechasalida);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló el guardado " + ex.Message);
            }
        }

        public void ActualizarHospedaje(Hospedaje hospedaje)
        {
            try
            {
                string consulta = string.Format("update hospedaje set participante_numasociado = '{0}', hotel_nombre = '{1}', fechaentrada = '{2}', fechasalida = '{3}' where idhospedaje = {4}", hospedaje.Participante_numasociado, hospedaje.Hotel_nombre, hospedaje.Fechaentrada, hospedaje.Fechasalida, hospedaje.Idhospedaje);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la actualización " + ex.Message);
            }
        }

        public void EliminarHospedaje(string hospedaje)
        {
            try
            {
                string consulta = string.Format("delete from hospedaje where idhospedaje = {0}", hospedaje);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la eliminación " + ex.Message);
            }
        }

        public List<Hospedaje> ObtenerHospedaje(string filtro)
        {
            var listaHospedaje = new List<Hospedaje>();
            var ds = new DataSet();
            string consulta = string.Format("select * from hospedaje where idhospedaje like '%{0}%'", filtro);
            ds = _conexion.ObtenerDatos(consulta, "hospedaje");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var hospedaje = new Hospedaje
                {
                    Idhospedaje = int.Parse(row["idhospedaje"].ToString()),
                    Participante_numasociado = row["participante_numasociado"].ToString(),
                    Hotel_nombre = row["hotel_nombre"].ToString(),
                    Fechaentrada = row["fechaentrada"].ToString(),
                    Fechasalida = row["fechasalida"].ToString()
                };
                listaHospedaje.Add(hospedaje);
            }
            return listaHospedaje;
        }
    }
}
