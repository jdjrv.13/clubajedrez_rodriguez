﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntidadesAjedrez;

namespace AccesoAjedrez
{
    public class HotelAccesoDatos
    {
        ConexiónBD _conexion;

        public HotelAccesoDatos()
        {
            try
            {
                _conexion = new ConexiónBD("localhost", "root", "", "clubajedrez", 3307);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la conexión " + ex.Message);
            }
        }

        public void GuardarHotel(Hotel hotel)
        {
            try
            {
                string consulta = string.Format("insert into hotel values('{0}', '{1}', '{2}')", hotel.Nombre, hotel.Direccion, hotel.Telefono);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló el guardado " + ex.Message);
            }
        }

        public void ActualizarHotel(Hotel hotel)
        {
            try
            {
                string consulta = string.Format("update hotel set direccion = '{0}', telefono = '{1}' where nombre = '{2}'", hotel.Direccion, hotel.Telefono, hotel.Nombre);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la actualización " + ex.Message);
            }
        }

        public void EliminarHotel(string hotel)
        {
            try
            {
                string consulta = string.Format("delete from hotel where nombre = '{0}'", hotel);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la eliminación " + ex.Message);
            }
        }

        public List<Hotel> ObtenerHotel(string filtro)
        {
            var listaHotel = new List<Hotel>();
            var ds = new DataSet();
            string consulta = string.Format("select * from hotel where nombre like '%{0}%'", filtro);
            ds = _conexion.ObtenerDatos(consulta, "hotel");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var hotel = new Hotel
                {
                    Nombre = row["nombre"].ToString(),
                    Direccion = row["direccion"].ToString(),
                    Telefono = row["telefono"].ToString()
                };
                listaHotel.Add(hotel);
            }
            return listaHotel;
        }
    }
}
