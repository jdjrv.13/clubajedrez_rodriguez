﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntidadesAjedrez;

namespace AccesoAjedrez
{
    public class MovimientoAccesoDatos
    {
        ConexiónBD _conexion;
        int c = 2;
        public MovimientoAccesoDatos()
        {
            try
            {
                _conexion = new ConexiónBD("localhost", "root", "", "clubajedrez", 3307);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la conexión " + ex.Message);
            }
        }

        public void GuardarMovimiento(Movimientos movimiento)
        {
            try
            {
                c++;
                string consulta = string.Format("insert into movimientos values('{0}',"+ c + ",'{1}', '{2}')", movimiento.Partidas_codp, movimiento.Movimiento, movimiento.Comentario);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló el guardado " + ex.Message);
            }
        }

        public void ActualizarMovimiento(Movimientos movimiento)
        {
            try
            {
                string consulta = string.Format("update movimientos set partidas_codp = '{0}', movimiento = '{1}', comentario = '{2}' where idmovimiento = {3}", movimiento.Partidas_codp, movimiento.Movimiento, movimiento.Comentario, movimiento.Idmovimiento);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la actualización " + ex.Message);
            }
        }

        public void EliminarMovimiento(string movimiento)
        {
            try
            {
                string consulta = string.Format("delete from movimientos where idmovimiento = {0}", movimiento);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la eliminación " + ex.Message);
            }
        }

        public List<Movimientos> ObtenerMovimiento(string filtro)
        {
            var listaMovimiento = new List<Movimientos>();
            var ds = new DataSet();
            string consulta = string.Format("select * from movimientos where partidas_codp like '%{0}%'", filtro);
            ds = _conexion.ObtenerDatos(consulta, "movimientos");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var movimiento = new Movimientos
                {
                    Partidas_codp = row["partidas_codp"].ToString(),
                    Idmovimiento = int.Parse(row["idmovimiento"].ToString()),
                    Movimiento = row["movimiento"].ToString(),
                    Comentario = row["comentario"].ToString()
                };
                listaMovimiento.Add(movimiento);
            }
            return listaMovimiento;
        }
    }
}
