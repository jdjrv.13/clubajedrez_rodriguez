﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntidadesAjedrez;

namespace AccesoAjedrez
{
    public class SalaAccesoDatos
    {
        ConexiónBD _conexion;

        public SalaAccesoDatos()
        {
            try
            {
                _conexion = new ConexiónBD("localhost", "root", "", "clubajedrez", 3307);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la conexión " + ex.Message);
            }
        }

        public void GuardarSala(Sala sala)
        {
            try
            {
                string consulta = string.Format("insert into sala values(null, {0}, '{1}', '{2}')", sala.Capacidad, sala.Medios, sala.Hotel_nombre);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló el guardado " + ex.Message);
            }
        }

        public void ActualizarSala(Sala sala)
        {
            try
            {
                string consulta = string.Format("update sala set capacidad = {0}, medios = '{1}', hotel_nombre = '{2}' where idsala = {3}", sala.Capacidad, sala.Medios, sala.Hotel_nombre, sala.Idsala);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la actualización " + ex.Message);
            }
        }

        public void EliminarSala(string sala)
        {
            try
            {
                string consulta = string.Format("delete from sala where idsala = {0}", sala);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la eliminación " + ex.Message);
            }
        }

        public List<Sala> ObtenerSala(string filtro)
        {
            var listaSala = new List<Sala>();
            var ds = new DataSet();
            string consulta = string.Format("select * from sala where idsala like '%{0}%'", filtro);
            ds = _conexion.ObtenerDatos(consulta, "sala");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var sala = new Sala
                {
                    Idsala = int.Parse(row["idsala"].ToString()),
                    Capacidad = int.Parse(row["capacidad"].ToString()),
                    Medios = row["medios"].ToString(),
                    Hotel_nombre = row["hotel_nombre"].ToString()
                };
                listaSala.Add(sala);
            }
            return listaSala;
        }
    }
}
