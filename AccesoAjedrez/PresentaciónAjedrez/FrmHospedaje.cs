﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LógicaNegocioAjedrez;
using EntidadesAjedrez;

namespace PresentaciónAjedrez
{
    public partial class FrmHospedaje : Form
    {
        private HospedajeManejador _hospedajeManejador;
        private Hospedaje _hospedaje;
        private string banderaguardar;

        public FrmHospedaje()
        {
            InitializeComponent();
            _hospedajeManejador = new HospedajeManejador();
            _hospedaje = new Hospedaje();
        }

        private void Botonera(Boolean nuevo, Boolean guardar, Boolean cancelar, Boolean eliminar)
        {
            btnNuevo.Enabled = nuevo;
            btnGuardar.Enabled = guardar;
            btncancelar.Enabled = cancelar;
            btnEliminar.Enabled = eliminar;
        }

        private void LimpiarCuadros()
        {
            txtAsociado.Text = "";
            txtHotel.Text = "";
            txtEntrada.Text = "";
            txtSalida.Text = "";
        }

        private void Cuadros(bool niv, bool cam, bool num, bool en)
        {
            txtAsociado.Enabled = niv;
            txtHotel.Enabled = cam;
            txtEntrada.Enabled = num;
            txtSalida.Enabled = en;
        }

        private void Buscarhospedaje(string s)
        {
            dgvHospedaje.DataSource = _hospedajeManejador.ObtenerHospedaje(s);
        }

        private void GuardarHospedaje()
        {
            Hospedaje hospedaje = new Hospedaje();
            hospedaje.Participante_numasociado = txtAsociado.Text;
            hospedaje.Hotel_nombre = txtHotel.Text;
            hospedaje.Fechaentrada = txtEntrada.Text;
            hospedaje.Fechasalida = txtSalida.Text;

            var valida = _hospedajeManejador.ValidarHospedaje(hospedaje);

            if (valida.Item1)
            {
                _hospedajeManejador.GuardarHospedaje(hospedaje);
                MessageBox.Show("El registro de hospedaje se guardó correctamente");
            }
            else
            {
                MessageBox.Show(valida.Item2, "Ocurrió un error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Actualizar()
        {
            _hospedajeManejador.ActualizarHospedaje(new Hospedaje
            {
                Idhospedaje = int.Parse(dgvHospedaje.CurrentRow.Cells["idhospedaje"].Value.ToString()),
                Participante_numasociado = txtAsociado.Text,
                Hotel_nombre = txtHotel.Text,
                Fechaentrada = txtEntrada.Text,
                Fechasalida = txtSalida.Text
            });
        }

        private void Eliminar()
        {
            var idhospedaje = dgvHospedaje.CurrentRow.Cells["idhospedaje"].Value.ToString();
            _hospedajeManejador.EliminarHospedaje(idhospedaje);
        }

        private void dgvJugador_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Botonera(false, true, true, false);
            Cuadros(true, true, true, true);
            txtAsociado.Focus();
            txtAsociado.Text = dgvHospedaje.CurrentRow.Cells["participante_numasociado"].Value.ToString();
            txtHotel.Text = dgvHospedaje.CurrentRow.Cells["hotel_nombre"].Value.ToString();
            txtEntrada.Text = dgvHospedaje.CurrentRow.Cells["fechaentrada"].Value.ToString();
            txtSalida.Text = dgvHospedaje.CurrentRow.Cells["fechasalida"].Value.ToString();
            banderaguardar = "actualizar";
        }

        private void FrmHospedaje_Load(object sender, EventArgs e)
        {
            Cuadros(false, false, false, false);
            Buscarhospedaje("");
            Botonera(true, false, false, true);
            LimpiarCuadros();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Botonera(false, true, true, false);
            Cuadros(true, true, true, true);
            txtAsociado.Focus();
            banderaguardar = "guardar";
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (banderaguardar == "guardar")
            {
                GuardarHospedaje();
                Buscarhospedaje("");
            }
            else
            {
                Actualizar();
                Buscarhospedaje("");
            }
            Botonera(true, false, false, true);
            LimpiarCuadros();
            Cuadros(false, false, false, false);
        }

        private void btncancelar_Click(object sender, EventArgs e)
        {
            Botonera(true, false, false, true);
            LimpiarCuadros();
            Cuadros(false, false, false, false);
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Desea eliminar el registro de hospedaje actual", "Eliminar", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Eliminar();
                Buscarhospedaje("");
                MessageBox.Show("Registro de hospedaje Eliminado");
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            Buscarhospedaje(textBox1.Text);
        }
    }
}
