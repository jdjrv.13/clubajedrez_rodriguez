﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LógicaNegocioAjedrez;
using EntidadesAjedrez;

namespace PresentaciónAjedrez
{
    public partial class FrmMovimientos : Form
    {
        private MovimientoManejador _movimientoManejador;
        private Movimientos _movimiento;
        private string banderaguardar;

        public FrmMovimientos()
        {
            InitializeComponent();
            _movimientoManejador = new MovimientoManejador();
            _movimiento = new Movimientos();
        }

        private void dgvJugador_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Botonera(false, true, true, false);
            Cuadros(true, true, true);
            txtPartida.Focus();
            txtPartida.Text = dgvJugador.CurrentRow.Cells["partidas_codp"].Value.ToString();
            txtMovimiento.Text = dgvJugador.CurrentRow.Cells["movimiento"].Value.ToString();
            txtComentario.Text = dgvJugador.CurrentRow.Cells["comentario"].Value.ToString();
            banderaguardar = "actualizar";
        }

        private void Botonera(Boolean nuevo, Boolean guardar, Boolean cancelar, Boolean eliminar)
        {
            btnNuevo.Enabled = nuevo;
            btnGuardar.Enabled = guardar;
            btncancelar.Enabled = cancelar;
            btnEliminar.Enabled = eliminar;
        }

        private void LimpiarCuadros()
        {
            txtPartida.Text = "";
            txtMovimiento.Text = "";
            txtComentario.Text = "";
        }

        private void Cuadros(bool niv, bool cam, bool num)
        {
            txtPartida.Enabled = niv;
            txtMovimiento.Enabled = cam;
            txtComentario.Enabled = num;
        }

        private void Buscarmovimiento(string s)
        {
            dgvJugador.DataSource = _movimientoManejador.ObtenerMovimiento(s);
        }

        private void GuardarMovimiento()
        {
            Movimientos movimiento = new Movimientos();
            movimiento.Partidas_codp = txtPartida.Text;
            movimiento.Movimiento = txtMovimiento.Text;
            movimiento.Comentario = txtComentario.Text;
            
            var valida = _movimientoManejador.ValidarMovimiento(movimiento);

            if (valida.Item1)
            {
                _movimientoManejador.GuardarMovimiento(movimiento);
                MessageBox.Show("El movimiento se guardó correctamente");
            }
            else
            {
                MessageBox.Show(valida.Item2, "Ocurrió un error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Actualizar()
        {
            _movimientoManejador.ActualizarMovimiento(new Movimientos
            {
                Partidas_codp = txtPartida.Text,
                Idmovimiento = int.Parse(dgvJugador.CurrentRow.Cells["idmovimiento"].Value.ToString()),
                Movimiento = txtMovimiento.Text,
                Comentario = txtComentario.Text
            });
        }

        private void Eliminar()
        {
            var idjugador = dgvJugador.CurrentRow.Cells["idmovimiento"].Value.ToString();
            _movimientoManejador.EliminarMovimiento(idjugador);
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Botonera(false, true, true, false);
            Cuadros(true, true, true);
            txtPartida.Focus();
            banderaguardar = "guardar";
        }

        private void FrmMovimientos_Load(object sender, EventArgs e)
        {
            Cuadros(false, false, false);
            Buscarmovimiento("");
            Botonera(true, false, false, true);
            LimpiarCuadros();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (banderaguardar == "guardar")
            {
                GuardarMovimiento();
                Buscarmovimiento("");
            }
            else
            {
                Actualizar();
                Buscarmovimiento("");
            }
            Botonera(true, false, false, true);
            LimpiarCuadros();
            Cuadros(false, false, false);
        }

        private void btncancelar_Click(object sender, EventArgs e)
        {
            Botonera(true, false, false, true);
            LimpiarCuadros();
            Cuadros(false, false, false);
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Desea eliminar el movimiento actual", "Eliminar", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Eliminar();
                Buscarmovimiento("");
                MessageBox.Show("Movimiento Eliminado");
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            Buscarmovimiento(textBox1.Text);
        }
    }
}
