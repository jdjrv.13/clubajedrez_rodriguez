﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LógicaNegocioAjedrez;
using EntidadesAjedrez;

namespace PresentaciónAjedrez
{
    public partial class FrmColor : Form
    {
        private ColorManejador _colorManejador;
        private Color1 _color;
        private string banderaguardar;

        public FrmColor()
        {
            InitializeComponent();
            _colorManejador = new ColorManejador();
            _color = new Color1();
        }

        private void dgvJugador_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Botonera(false, true, true, false);
            Cuadros(true, true, true);
            txtJugador.Focus();
            txtJugador.Text = dgvJugador.CurrentRow.Cells["jugador_idjugador"].Value.ToString();
            txtColor.Text = dgvJugador.CurrentRow.Cells["color"].Value.ToString();
            txtPartida.Text = dgvJugador.CurrentRow.Cells["partidas_codp"].Value.ToString();
            banderaguardar = "actualizar";
        }
        private void Botonera(Boolean nuevo, Boolean guardar, Boolean cancelar, Boolean eliminar)
        {
            btnNuevo.Enabled = nuevo;
            btnGuardar.Enabled = guardar;
            btncancelar.Enabled = cancelar;
            btnEliminar.Enabled = eliminar;
        }

        private void LimpiarCuadros()
        {
            txtJugador.Text = "";
            txtColor.Text = "";
            txtPartida.Text = "";
        }

        private void Cuadros(bool niv, bool cam, bool num)
        {
            txtJugador.Enabled = niv;
            txtColor.Enabled = cam;
            txtPartida.Enabled = num;
        }

        private void Buscarcolor(string s)
        {
            dgvJugador.DataSource = _colorManejador.ObtenerColor(s);
        }

        private void GuardarColor()
        {
            Color1 color = new Color1();
            color.Jugador_idjugador = int.Parse(txtJugador.Text);
            color.Color = txtColor.Text;
            color.Partidas_codp = txtPartida.Text;

            var valida = _colorManejador.ValidarColor(color);

            if (valida.Item1)
            {
                _colorManejador.GuardarColor(color);
                MessageBox.Show("El registro se guardó correctamente");
            }
            else
            {
                MessageBox.Show(valida.Item2, "Ocurrió un error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Actualizar()
        {
            _colorManejador.ActualizarColor(new Color1
            {
                Idcolor = int.Parse(dgvJugador.CurrentRow.Cells["idcolor"].Value.ToString()),
                Jugador_idjugador = int.Parse(txtJugador.Text),
                Color = txtColor.Text,
                Partidas_codp = txtPartida.Text
            });
        }

        private void Eliminar()
        {
            var idcolor = dgvJugador.CurrentRow.Cells["idcolor"].Value.ToString();
            _colorManejador.EliminarColor(idcolor);
        }

        private void FrmColor_Load(object sender, EventArgs e)
        {
            Cuadros(false, false, false);
            Buscarcolor("");
            Botonera(true, false, false, true);
            LimpiarCuadros();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Botonera(false, true, true, false);
            Cuadros(true, true, true);
            txtJugador.Focus();
            banderaguardar = "guardar";
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (banderaguardar == "guardar")
            {
                GuardarColor();
                Buscarcolor("");
            }
            else
            {
                Actualizar();
                Buscarcolor("");
            }
            Botonera(true, false, false, true);
            LimpiarCuadros();
            Cuadros(false, false, false);
        }

        private void btncancelar_Click(object sender, EventArgs e)
        {
            Botonera(true, false, false, true);
            LimpiarCuadros();
            Cuadros(false, false, false);
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Desea eliminar el color actual", "Eliminar", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Eliminar();
                Buscarcolor("");
                MessageBox.Show("Jugador Eliminado");
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            Buscarcolor(textBox1.Text);
        }
    }
}
