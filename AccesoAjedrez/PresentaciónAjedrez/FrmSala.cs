﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LógicaNegocioAjedrez;
using EntidadesAjedrez;

namespace PresentaciónAjedrez
{
    public partial class FrmSala : Form
    {
        private SalaManejador _salaManejador;
        private Sala _sala;
        private string banderaguardar;

        public FrmSala()
        {
            InitializeComponent();
            _salaManejador = new SalaManejador();
            _sala = new Sala();
        }

        private void dgvJugador_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Botonera(false, true, true, false);
            Cuadros(true, true, true);
            txtCapacidad.Focus();
            txtCapacidad.Text = dgvJugador.CurrentRow.Cells["capacidad"].Value.ToString();
            txtMedios.Text = dgvJugador.CurrentRow.Cells["medios"].Value.ToString();
            txtHotel.Text = dgvJugador.CurrentRow.Cells["hotel_nombre"].Value.ToString();
            banderaguardar = "actualizar";
        }

        private void Botonera(Boolean nuevo, Boolean guardar, Boolean cancelar, Boolean eliminar)
        {
            btnNuevo.Enabled = nuevo;
            btnGuardar.Enabled = guardar;
            btncancelar.Enabled = cancelar;
            btnEliminar.Enabled = eliminar;
        }

        private void LimpiarCuadros()
        {
            txtCapacidad.Text = "";
            txtMedios.Text = "";
            txtHotel.Text = "";
        }

        private void Cuadros(bool niv, bool cam, bool num)
        {
            txtCapacidad.Enabled = niv;
            txtMedios.Enabled = cam;
            txtHotel.Enabled = num;
        }

        private void Buscarsala(string s)
        {
            dgvJugador.DataSource = _salaManejador.ObtenerSala(s);
        }

        private void GuardarSala()
        {
            Sala sala = new Sala();
            sala.Capacidad = int.Parse(txtCapacidad.Text);
            sala.Medios = txtMedios.Text;
            sala.Hotel_nombre = txtHotel.Text;

            var valida = _salaManejador.ValidarSala(sala);

            if (valida.Item1)
            {
                _salaManejador.GuardarSala(sala);
                MessageBox.Show("La sala se guardó correctamente");
            }
            else
            {
                MessageBox.Show(valida.Item2, "Ocurrió un error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Actualizar()
        {
            _salaManejador.ActualizarSala(new Sala
            {
                Idsala = int.Parse(dgvJugador.CurrentRow.Cells["idsala"].Value.ToString()),
                Capacidad = int.Parse(txtCapacidad.Text),
                Medios = txtMedios.Text,
                Hotel_nombre = txtHotel.Text
            });
        }

        private void Eliminar()
        {
            var idsala = dgvJugador.CurrentRow.Cells["idsala"].Value.ToString();
            _salaManejador.EliminarSala(idsala);
        }

        private void FrmSala_Load(object sender, EventArgs e)
        {
            Cuadros(false, false, false);
            Buscarsala("");
            Botonera(true, false, false, true);
            LimpiarCuadros();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Botonera(false, true, true, false);
            Cuadros(true, true, true);
            txtCapacidad.Focus();
            banderaguardar = "guardar";
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (banderaguardar == "guardar")
            {
                GuardarSala();
                Buscarsala("");
            }
            else
            {
                Actualizar();
                Buscarsala("");
            }
            Botonera(true, false, false, true);
            LimpiarCuadros();
            Cuadros(false, false, false);
        }

        private void btncancelar_Click(object sender, EventArgs e)
        {
            Botonera(true, false, false, true);
            LimpiarCuadros();
            Cuadros(false, false, false);
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Desea eliminar la sala actual", "Eliminar", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Eliminar();
                Buscarsala("");
                MessageBox.Show("Sala Eliminado");
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            Buscarsala(textBox1.Text);
        }
    }
}
