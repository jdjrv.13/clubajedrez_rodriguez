﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LógicaNegocioAjedrez;
using EntidadesAjedrez;

namespace PresentaciónAjedrez
{
    public partial class FrmJugador : Form
    {
        private JugadorManejador _jugadorManejador;
        private Jugador _jugador;
        private string banderaguardar;

        public FrmJugador()
        {
            InitializeComponent();
            _jugadorManejador = new JugadorManejador();
            _jugador = new Jugador();
        }

        private void dgvPaís_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Botonera(false, true, true, false);
            Cuadros(true, true, true);
            txtNivel.Focus();
            txtNivel.Text = dgvJugador.CurrentRow.Cells["nivel"].Value.ToString();
            txtCamp.Text = dgvJugador.CurrentRow.Cells["campjugados"].Value.ToString();
            txtNumaso.Text = dgvJugador.CurrentRow.Cells["participante_numasociado"].Value.ToString();
            banderaguardar = "actualizar";
        }

        private void Botonera(Boolean nuevo, Boolean guardar, Boolean cancelar, Boolean eliminar)
        {
            btnNuevo.Enabled = nuevo;
            btnGuardar.Enabled = guardar;
            btncancelar.Enabled = cancelar;
            btnEliminar.Enabled = eliminar;
        }

        private void LimpiarCuadros()
        {
            txtNivel.Text = "";
            txtCamp.Text = "";
            txtNumaso.Text = "";
        }

        private void Cuadros(bool niv, bool cam, bool num)
        {
            txtNivel.Enabled = niv;
            txtCamp.Enabled = cam;
            txtNumaso.Enabled = num;
        }

        private void Buscarjugador(string s)
        {
            dgvJugador.DataSource = _jugadorManejador.ObtenerJugador(s);
        }

        private void GuardarJugador()
        {
            Jugador jugador = new Jugador();
            jugador.Nivel = int.Parse(txtNivel.Text);
            jugador.Campjugados = int.Parse(txtCamp.Text);
            jugador.Participante_numasociado = txtNumaso.Text;

            var valida = _jugadorManejador.ValidarJugador(jugador);

            if (valida.Item1)
            {
                _jugadorManejador.GuardarJugador(jugador);
                MessageBox.Show("El jugador se guardó correctamente");
            }
            else
            {
                MessageBox.Show(valida.Item2, "Ocurrió un error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Actualizar()
        {
            _jugadorManejador.ActualizarJugador(new Jugador
            {
                Idjugador = int.Parse(dgvJugador.CurrentRow.Cells["idjugador"].Value.ToString()),
                Nivel = int.Parse(txtNivel.Text),
                Campjugados = int.Parse(txtCamp.Text),
                Participante_numasociado = txtNumaso.Text
            });
        }

        private void Eliminar()
        {
            var idjugador = dgvJugador.CurrentRow.Cells["idjugador"].Value.ToString();
            _jugadorManejador.EliminarJugador(idjugador);
        }

        private void FrmJugador_Load(object sender, EventArgs e)
        {
            Cuadros(false, false, false);
            Buscarjugador("");
            Botonera(true, false, false, true);
            LimpiarCuadros();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Botonera(false, true, true, false);
            Cuadros(true, true, true);
            txtNivel.Focus();
            banderaguardar = "guardar";
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (banderaguardar == "guardar")
            {
                GuardarJugador();
                Buscarjugador("");
            }
            else
            {
                Actualizar();
                Buscarjugador("");
            }
            Botonera(true, false, false, true);
            LimpiarCuadros();
            Cuadros(false, false, false);
        }

        private void btncancelar_Click(object sender, EventArgs e)
        {
            Botonera(true, false, false, true);
            LimpiarCuadros();
            Cuadros(false, false, false);
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Desea eliminar el jugador actual", "Eliminar", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Eliminar();
                Buscarjugador("");
                MessageBox.Show("Jugador Eliminado");
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            Buscarjugador(textBox1.Text);
        }
    }
}
