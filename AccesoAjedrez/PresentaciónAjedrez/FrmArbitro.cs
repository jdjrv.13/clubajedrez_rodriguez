﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LógicaNegocioAjedrez;
using EntidadesAjedrez;

namespace PresentaciónAjedrez
{
    public partial class FrmArbitro : Form
    {
        private ArbitroManejador _arbitroManejador;
        private Árbitro _arbitro;
        private string banderaguardar;
        public FrmArbitro()
        {
            InitializeComponent();
            _arbitroManejador = new ArbitroManejador();
            _arbitro = new Árbitro();
        }

        private void FrmArbitro_Load(object sender, EventArgs e)
        {
            Cuadros(false, false);
            BuscarArbitro("");
            Botonera(true, false, false, true);
            LimpiarCuadros();
        }

        private void dgvJugador_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Botonera(false, true, true, false);
            Cuadros(true, true);
            txtCamp.Focus();
            txtCamp.Text = dgvJugador.CurrentRow.Cells["campjugados"].Value.ToString();
            txtNumaso.Text = dgvJugador.CurrentRow.Cells["participante_numasociado"].Value.ToString();
            banderaguardar = "actualizar";
        }

        private void Botonera(Boolean nuevo, Boolean guardar, Boolean cancelar, Boolean eliminar)
        {
            btnNuevo.Enabled = nuevo;
            btnGuardar.Enabled = guardar;
            btncancelar.Enabled = cancelar;
            btnEliminar.Enabled = eliminar;
        }

        private void LimpiarCuadros()
        {
            txtCamp.Text = "";
            txtNumaso.Text = "";
        }

        private void Cuadros(bool cam, bool num)
        {
            txtCamp.Enabled = cam;
            txtNumaso.Enabled = num;
        }

        private void BuscarArbitro(string s)
        {
            dgvJugador.DataSource = _arbitroManejador.ObtenerArbitro(s);
        }

        private void GuardarArbitro()
        {
            Árbitro arbitro = new Árbitro();
            arbitro.Campjugados = int.Parse(txtCamp.Text);
            arbitro.Participante_numasociado = txtNumaso.Text;

            var valida = _arbitroManejador.ValidarArbitro(arbitro);

            if (valida.Item1)
            {
                _arbitroManejador.GuardarArbitro(arbitro);
                MessageBox.Show("El arbitro se guardó correctamente");
            }
            else
            {
                MessageBox.Show(valida.Item2, "Ocurrió un error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Actualizar()
        {
            _arbitroManejador.ActualizarArbitro(new Árbitro
            {
                Idarbitro = int.Parse(dgvJugador.CurrentRow.Cells["idarbitro"].Value.ToString()),
                Campjugados = int.Parse(txtCamp.Text),
                Participante_numasociado = txtNumaso.Text
            });
        }

        private void Eliminar()
        {
            var idarbitro = dgvJugador.CurrentRow.Cells["idarbitro"].Value.ToString();
            _arbitroManejador.EliminarArbitro(idarbitro);
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Botonera(false, true, true, false);
            Cuadros(true, true);
            txtCamp.Focus();
            banderaguardar = "guardar";
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (banderaguardar == "guardar")
            {
                GuardarArbitro();
                BuscarArbitro("");
            }
            else
            {
                Actualizar();
                BuscarArbitro("");
            }
            Botonera(true, false, false, true);
            LimpiarCuadros();
            Cuadros(false, false);
        }

        private void btncancelar_Click(object sender, EventArgs e)
        {
            Botonera(true, false, false, true);
            LimpiarCuadros();
            Cuadros(false, false);
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Desea eliminar el arbitro actual", "Eliminar", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Eliminar();
                BuscarArbitro("");
                MessageBox.Show("Arbitro Eliminado");
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            BuscarArbitro(textBox1.Text);
        }
    }
}
