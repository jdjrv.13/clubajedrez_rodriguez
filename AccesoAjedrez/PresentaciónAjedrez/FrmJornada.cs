﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LógicaNegocioAjedrez;
using EntidadesAjedrez;

namespace PresentaciónAjedrez
{
    public partial class FrmJornada : Form
    {
        private JornadaManejador _jornadaManejador;
        private Jornada _jornada;
        private string banderaguardar;

        public FrmJornada()
        {
            InitializeComponent();
            _jornadaManejador = new JornadaManejador();
            _jornada = new Jornada();
        }

        private void dgvJornada_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Botonera(false, true, true, false);
            Cuadros(true, true);
            txtPerido.Focus();
            txtPerido.Text = dgvJornada.CurrentRow.Cells["perido"].Value.ToString();
            txtAño.Text = dgvJornada.CurrentRow.Cells["año"].Value.ToString();
            banderaguardar = "actualizar";
        }

        private void Botonera(Boolean nuevo, Boolean guardar, Boolean cancelar, Boolean eliminar)
        {
            btnNuevo.Enabled = nuevo;
            btnGuardar.Enabled = guardar;
            btncancelar.Enabled = cancelar;
            btnEliminar.Enabled = eliminar;
        }

        private void LimpiarCuadros()
        {
            txtPerido.Text = "";
            txtAño.Text = "";
        }

        private void Cuadros(bool niv, bool cam)
        {
            txtPerido.Enabled = niv;
            txtAño.Enabled = cam;
        }

        private void Buscarjornada(string s)
        {
            dgvJornada.DataSource = _jornadaManejador.ObtenerJornada(s);
        }

        private void GuardarJornada()
        {
            Jornada jornada = new Jornada();
            jornada.Perido = txtPerido.Text;
            jornada.Año = txtAño.Text;

            var valida = _jornadaManejador.ValidarJornada(jornada);

            if (valida.Item1)
            {
                _jornadaManejador.GuardarJornada(jornada);
                MessageBox.Show("La jornada se guardó correctamente");
            }
            else
            {
                MessageBox.Show(valida.Item2, "Ocurrió un error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Actualizar()
        {
            _jornadaManejador.ActualizarJornada(new Jornada
            {
                Idjornada = int.Parse(dgvJornada.CurrentRow.Cells["idjornada"].Value.ToString()),
                Perido = txtPerido.Text,
                Año = txtAño.Text
            });
        }

        private void Eliminar()
        {
            var idjornada = dgvJornada.CurrentRow.Cells["idjornada"].Value.ToString();
            _jornadaManejador.EliminarJornada(idjornada);
        }

        private void FrmJornada_Load(object sender, EventArgs e)
        {
            Cuadros(false, false);
            Buscarjornada("");
            Botonera(true, false, false, true);
            LimpiarCuadros();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Botonera(false, true, true, false);
            Cuadros(true, true);
            txtPerido.Focus();
            banderaguardar = "guardar";
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (banderaguardar == "guardar")
            {
                GuardarJornada();
                Buscarjornada("");
            }
            else
            {
                Actualizar();
                Buscarjornada("");
            }
            Botonera(true, false, false, true);
            LimpiarCuadros();
            Cuadros(false, false);
        }

        private void btncancelar_Click(object sender, EventArgs e)
        {
            Botonera(true, false, false, true);
            LimpiarCuadros();
            Cuadros(false, false);
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Desea eliminar la jornada actual", "Eliminar", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Eliminar();
                Buscarjornada("");
                MessageBox.Show("Jornada Eliminada");
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            Buscarjornada(textBox1.Text);
        }
    }
}
