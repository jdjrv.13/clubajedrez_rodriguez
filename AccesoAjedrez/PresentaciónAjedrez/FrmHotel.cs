﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EntidadesAjedrez;
using LógicaNegocioAjedrez;

namespace PresentaciónAjedrez
{
    public partial class FrmHotel : Form
    {
        private HotelManejador _hotelManejador;
        private Hotel _hotel;
        private string banderaguardar;
        public FrmHotel()
        {
            InitializeComponent();
            _hotelManejador = new HotelManejador();
            _hotel = new Hotel();
        }

        private void dgvPaís_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Botonera(false, true, true, false);
            Cuadros(false, true, true);
            txtDireccion.Focus();
            txtNombre.Text = dgvHotel.CurrentRow.Cells["nombre"].Value.ToString();
            txtDireccion.Text = dgvHotel.CurrentRow.Cells["direccion"].Value.ToString();
            txtTel.Text = dgvHotel.CurrentRow.Cells["telefono"].Value.ToString();
            banderaguardar = "actualizar";
        }

        private void Botonera(Boolean nuevo, Boolean guardar, Boolean cancelar, Boolean eliminar)
        {
            btnNuevo.Enabled = nuevo;
            btnGuardar.Enabled = guardar;
            btncancelar.Enabled = cancelar;
            btnEliminar.Enabled = eliminar;
        }

        private void LimpiarCuadros()
        {
            txtNombre.Text = "";
            txtDireccion.Text = "";
            txtTel.Text = "";
        }

        private void Cuadros(bool nom, bool dir, bool tel)
        {
            txtNombre.Enabled = nom;
            txtDireccion.Enabled = dir;
            txtTel.Enabled = tel;
        }

        private void Buscarhotel(string s)
        {
            dgvHotel.DataSource = _hotelManejador.ObtenerHotel(s);
        }

        private void GuardarHotel()
        {
            Hotel hotel = new Hotel();
            hotel.Nombre = txtNombre.Text;
            hotel.Direccion = txtDireccion.Text;
            hotel.Telefono = txtTel.Text;

            var valida = _hotelManejador.ValidarHotel(hotel);

            if (valida.Item1)
            {
                _hotelManejador.GuardarHotel(hotel);
                MessageBox.Show("El hotel se guardó correctamente");
            }
            else
            {
                MessageBox.Show(valida.Item2, "Ocurrió un error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Actualizar()
        {
            _hotelManejador.ActualizarHotel(new Hotel
            {
                Nombre = txtNombre.Text,
                Direccion = txtDireccion.Text,
                Telefono = txtTel.Text
            });
        }

        private void Eliminar()
        {
            var idnombre= dgvHotel.CurrentRow.Cells["nombre"].Value.ToString();
            _hotelManejador.EliminarHotel(idnombre);
        }

        private void FrmHotel_Load(object sender, EventArgs e)
        {
            Cuadros(false, false, false);
            Buscarhotel("");
            Botonera(true, false, false, true);
            LimpiarCuadros();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Botonera(false, true, true, false);
            Cuadros(true, true, true);
            txtNombre.Focus();
            banderaguardar = "guardar";
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (banderaguardar == "guardar")
            {
                GuardarHotel();
                Buscarhotel("");
            }
            else
            {
                Actualizar();
                Buscarhotel("");
            }
            Botonera(true, false, false, true);
            LimpiarCuadros();
            Cuadros(false, false, false);
        }

        private void btncancelar_Click(object sender, EventArgs e)
        {
            Botonera(true, false, false, true);
            LimpiarCuadros();
            Cuadros(false, false, false);
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Desea eliminar el hotel actual", "Eliminar", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Eliminar();
                Buscarhotel("");
                MessageBox.Show("Hotel Eliminado");
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            Buscarhotel(textBox1.Text);
        }
    }
}
