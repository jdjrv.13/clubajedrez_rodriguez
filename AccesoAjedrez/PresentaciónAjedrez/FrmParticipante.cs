﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EntidadesAjedrez;
using LógicaNegocioAjedrez;

namespace PresentaciónAjedrez
{
    public partial class FrmParticipante : Form
    {
        private ParticipanteManejador _participanteManejador;
        private Participante _participante;
        private string banderaguardar;
        public FrmParticipante()
        {
            InitializeComponent();
            _participanteManejador = new ParticipanteManejador();
            _participante = new Participante();
        }

        private void dgvPaís_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Botonera(false, true, true, false);
            Cuadros(false, true, true, true, true);
            txtNombre.Focus();
            txtNumAso.Text = dgvParticipante.CurrentRow.Cells["numasociado"].Value.ToString();
            txtNombre.Text = dgvParticipante.CurrentRow.Cells["nombre"].Value.ToString();
            txtDirección.Text = dgvParticipante.CurrentRow.Cells["direccion"].Value.ToString();
            txtTeléfono.Text = dgvParticipante.CurrentRow.Cells["telefono"].Value.ToString();
            txtCorrelativo.Text = dgvParticipante.CurrentRow.Cells["pais_numcorrid"].Value.ToString();
            banderaguardar = "actualizar";
        }

        private void Botonera(Boolean nuevo, Boolean guardar, Boolean cancelar, Boolean eliminar)
        {
            btnNuevo.Enabled = nuevo;
            btnGuardar.Enabled = guardar;
            btncancelar.Enabled = cancelar;
            btnEliminar.Enabled = eliminar;
        }

        private void LimpiarCuadros()
        {
            txtNumAso.Text = "";
            txtNombre.Text = "";
            txtDirección.Text = "";
            txtTeléfono.Text = "";
            txtCorrelativo.Text = "";
        }

        private void Cuadros(bool num, bool nom, bool dir, bool tel, bool corr)
        {
            txtNumAso.Enabled = num;
            txtNombre.Enabled = nom;
            txtDirección.Enabled = dir;
            txtTeléfono.Enabled = tel;
            txtCorrelativo.Enabled = corr;
        }

        private void Buscarparticipante(string s)
        {
            dgvParticipante.DataSource = _participanteManejador.ObtenerParticipante(s);
        }

        private void GuardarParticipante()
        {
            Participante participante = new Participante();
            participante.Numasociado = txtNumAso.Text;
            participante.Nombre = txtNombre.Text;
            participante.Direccion = txtDirección.Text;
            participante.Telefono = txtTeléfono.Text;
            participante.Pais_numcorrid = txtCorrelativo.Text;

            var valida = _participanteManejador.ValidarParticipante(participante);

            if (valida.Item1)
            {
                _participanteManejador.GuardarParticipante(participante);
                MessageBox.Show("El participante se guardó correctamente");
            }
            else
            {
                MessageBox.Show(valida.Item2, "Ocurrió un error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Actualizar()
        {
            _participanteManejador.ActualizarParticipante(new Participante
            {
                Numasociado = txtNumAso.Text,
                Nombre = txtNombre.Text,
                Direccion = txtDirección.Text,
                Telefono = txtTeléfono.Text,
                Pais_numcorrid = txtCorrelativo.Text
            });
        }

        private void Eliminar()
        {
            var idparticipante = dgvParticipante.CurrentRow.Cells["numasociado"].Value.ToString();
            _participanteManejador.EliminarParticipante(idparticipante);
        }

        private void FrmParticipante_Load(object sender, EventArgs e)
        {
            Cuadros(false, false, false, false, false);
            Buscarparticipante("");
            Botonera(true, false, false, true);
            LimpiarCuadros();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Botonera(false, true, true, false);
            Cuadros(true, true, true, true, true);
            txtNumAso.Focus();
            banderaguardar = "guardar";
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (banderaguardar == "guardar")
            {
                GuardarParticipante();
                Buscarparticipante("");
            }
            else
            {
                Actualizar();
                Buscarparticipante("");
            }
            Botonera(true, false, false, true);
            LimpiarCuadros();
            Cuadros(false, false, false, false, false);
        }

        private void btncancelar_Click(object sender, EventArgs e)
        {
            Botonera(true, false, false, true);
            LimpiarCuadros();
            Cuadros(false, false, false, false, false);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            Buscarparticipante(textBox1.Text);
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Desea eliminar el participante actual", "Eliminar", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Eliminar();
                Buscarparticipante("");
                MessageBox.Show("Participante Eliminado");
            }
        }
    }
}
