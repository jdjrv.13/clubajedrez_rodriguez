﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LógicaNegocioAjedrez;
using EntidadesAjedrez;

namespace PresentaciónAjedrez
{
    public partial class FrmPartida : Form
    {
        private PartidaManejador _partidaManejador;
        private Partida _partida;
        private string banderaguardar;

        public FrmPartida()
        {
            InitializeComponent();
            _partidaManejador = new PartidaManejador();
            _partida = new Partida();
        }

        private void FrmPartida_Load(object sender, EventArgs e)
        {
            Cuadros(false, false, false, false, false);
            Buscarpartida("");
            Botonera(true, false, false, true);
            LimpiarCuadros();
        }

        private void dgvJugador_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Botonera(false, true, true, false);
            Cuadros(false, true, true, true, true);
            txtArbitro.Focus();
            txtPartida.Text = dgvJugador.CurrentRow.Cells["codp"].Value.ToString();
            txtArbitro.Text = dgvJugador.CurrentRow.Cells["arbitro_idarbitro"].Value.ToString();
            txtJornada.Text = dgvJugador.CurrentRow.Cells["jornada_idjornada"].Value.ToString();
            txtSala.Text = dgvJugador.CurrentRow.Cells["sala_idsala"].Value.ToString();
            txtEntradas.Text = dgvJugador.CurrentRow.Cells["entradas"].Value.ToString();
            banderaguardar = "actualizar";
        }

        private void Botonera(Boolean nuevo, Boolean guardar, Boolean cancelar, Boolean eliminar)
        {
            btnNuevo.Enabled = nuevo;
            btnGuardar.Enabled = guardar;
            btncancelar.Enabled = cancelar;
            btnEliminar.Enabled = eliminar;
        }

        private void LimpiarCuadros()
        {
            txtPartida.Text = "";
            txtArbitro.Text = "";
            txtJornada.Text = "";
            txtSala.Text = "";
            txtEntradas.Text = "";
        }

        private void Cuadros(bool niv, bool cam, bool num, bool sal, bool ent)
        {
            txtPartida.Enabled = niv;
            txtArbitro.Enabled = cam;
            txtJornada.Enabled = num;
            txtSala.Enabled = sal;
            txtEntradas.Enabled = ent;
        }

        private void Buscarpartida(string s)
        {
            dgvJugador.DataSource = _partidaManejador.ObtenerPartida(s);
        }

        private void GuardarPartida()
        {
            Partida partida = new Partida();
            partida.Codp = txtPartida.Text;
            partida.Arbitro_idarbitro = int.Parse(txtArbitro.Text);
            partida.Jornada_idjornada = int.Parse(txtJornada.Text);
            partida.Sala_idsala = int.Parse(txtSala.Text);
            partida.Entradas = int.Parse(txtEntradas.Text);

            var valida = _partidaManejador.ValidarPartida(partida);

            if (valida.Item1)
            {
                _partidaManejador.GuardarPartida(partida);
                MessageBox.Show("La partida se guardó correctamente");
            }
            else
            {
                MessageBox.Show(valida.Item2, "Ocurrió un error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Actualizar()
        {
            _partidaManejador.ActualizarPartida(new Partida
            {
                Codp = txtPartida.Text,
                Arbitro_idarbitro = int.Parse(txtArbitro.Text),
                Jornada_idjornada = int.Parse(txtJornada.Text),
                Sala_idsala = int.Parse(txtSala.Text),
                Entradas = int.Parse(txtEntradas.Text)
            });
        }

        private void Eliminar()
        {
            var idpartida = dgvJugador.CurrentRow.Cells["codp"].Value.ToString();
            _partidaManejador.EliminarPartida(idpartida);
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Botonera(false, true, true, false);
            Cuadros(true, true, true, true, true);
            txtPartida.Focus();
            banderaguardar = "guardar";
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (banderaguardar == "guardar")
            {
                GuardarPartida();
                Buscarpartida("");
            }
            else
            {
                Actualizar();
                Buscarpartida("");
            }
            Botonera(true, false, false, true);
            LimpiarCuadros();
            Cuadros(false, false, false, false, false);
        }

        private void btncancelar_Click(object sender, EventArgs e)
        {
            Botonera(true, false, false, true);
            LimpiarCuadros();
            Cuadros(false, false, false, false, false);
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Desea eliminar la partida actual", "Eliminar", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Eliminar();
                Buscarpartida("");
                MessageBox.Show("Partida Eliminada");
            }
        }
    }
}
