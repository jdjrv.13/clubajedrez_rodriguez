﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EntidadesAjedrez;
using LógicaNegocioAjedrez;

namespace PresentaciónAjedrez
{
    public partial class FrmPaís : Form
    {
        private PaisManejador _paisManejador;
        private País _pais;
        private string banderaguardar;
        public FrmPaís()
        {
            InitializeComponent();
            _paisManejador = new PaisManejador();
            _pais = new País();
        }

        private void dgvUsuario_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Botonera(false, true, true, false);
            Cuadros(false, false, false, true);
            txtRep.Focus();
            txtNum.Text = dgvPaís.CurrentRow.Cells["numcorrid"].Value.ToString();
            txtNombre.Text = dgvPaís.CurrentRow.Cells["nombre"].Value.ToString();
            txtClub.Text = dgvPaís.CurrentRow.Cells["numclubes"].Value.ToString();
            txtRep.Text = dgvPaís.CurrentRow.Cells["representa"].Value.ToString();
            banderaguardar = "actualizar";
        }

        private void Botonera(Boolean nuevo, Boolean guardar, Boolean cancelar, Boolean eliminar)
        {
            btnNuevo.Enabled = nuevo;
            btnGuardar.Enabled = guardar;
            btncancelar.Enabled = cancelar;
            btnEliminar.Enabled = eliminar;
        }

        private void LimpiarCuadros()
        {
            txtNum.Text = "";
            txtNombre.Text = "";
            txtClub.Text = "";
            txtRep.Text = "";
        }

        private void Cuadros(bool num, bool nom, bool club, bool rep)
        {
            txtNum.Enabled = num;
            txtNombre.Enabled = nom;
            txtClub.Enabled = club;
            txtRep.Enabled = rep;
        }

        private void Buscarpais(string s)
        {
            dgvPaís.DataSource = _paisManejador.ObtenerPais(s);
        }

        private void GuardarPais()
        {
            País pais = new País();
            pais.Numcorrid = txtNum.Text;
            pais.Nombre = txtNombre.Text;
            pais.Numclubes = int.Parse(txtClub.Text);
            pais.Representa = txtRep.Text;

            var valida = _paisManejador.ValidarPais(pais);

            if (valida.Item1)
            {
                _paisManejador.GuardarPais(pais);
                MessageBox.Show("El País se guardó correctamente");
            }
            else
            {
                MessageBox.Show(valida.Item2, "Ocurrió un error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Actualizar()
        {
            _paisManejador.ActualizarPais(new País
            {
                Numcorrid = txtNum.Text,
                Nombre = txtNombre.Text,
                Numclubes = int.Parse(txtClub.Text),
                Representa = txtRep.Text
            });
        }

        private void Eliminar()
        {
            var idpais = dgvPaís.CurrentRow.Cells["numcorrid"].Value.ToString();
            _paisManejador.EliminarPais(idpais);
        }
        private void FrmPaís_Load(object sender, EventArgs e)
        {
            Cuadros(false, false, false, false);
            Buscarpais("");
            Botonera(true, false, false, true);
            LimpiarCuadros();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Botonera(false, true, true, false);
            Cuadros(true, true, true, true);
            txtRep.Focus();
            banderaguardar = "guardar";
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (banderaguardar == "guardar")
            {
                GuardarPais();
                Buscarpais("");
            }
            else
            {
                Actualizar();
                Buscarpais("");
            }
            Botonera(true, false, false, true);
            LimpiarCuadros();
            Cuadros(false, false, false, false);
        }

        private void btncancelar_Click(object sender, EventArgs e)
        {
            Botonera(true, false, false, true);
            LimpiarCuadros();
            Cuadros(false, false, false, false);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            Buscarpais(textBox1.Text);
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Desea eliminar el pais actual", "Eliminar", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Eliminar();
                Buscarpais("");
                MessageBox.Show("Pais Eliminado");
            }
        }
    }
}
