-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema clubajedrez
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema clubajedrez
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `clubajedrez` DEFAULT CHARACTER SET utf8 ;
USE `clubajedrez` ;

-- -----------------------------------------------------
-- Table `clubajedrez`.`pais`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `clubajedrez`.`pais` (
  `numcorrid` VARCHAR(4) NOT NULL,
  `nombre` VARCHAR(45) NULL,
  `numclubes` INT NULL,
  `representa` VARCHAR(4) NULL,
  PRIMARY KEY (`numcorrid`, `representa`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `clubajedrez`.`participante`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `clubajedrez`.`participante` (
  `numasociado` VARCHAR(6) NOT NULL,
  `nombre` VARCHAR(100) NULL,
  `direccion` VARCHAR(100) NULL,
  `telefono` VARCHAR(100) NULL,
  `pais_numcorrid` VARCHAR(4) NOT NULL,
  PRIMARY KEY (`numasociado`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `clubajedrez`.`jugador`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `clubajedrez`.`jugador` (
  `idjugador` INT NOT NULL AUTO_INCREMENT,
  `nivel` INT NULL,
  `campjugados` INT NULL,
  `participante_numasociado` VARCHAR(6) NOT NULL,
  PRIMARY KEY (`idjugador`, `participante_numasociado`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `clubajedrez`.`arbitro`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `clubajedrez`.`arbitro` (
  `idarbitro` INT NOT NULL AUTO_INCREMENT,
  `campjugados` INT NULL,
  `participante_numasociado` VARCHAR(6) NOT NULL,
  PRIMARY KEY (`idarbitro`, `participante_numasociado`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `clubajedrez`.`hotel`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `clubajedrez`.`hotel` (
  `nombre` VARCHAR(30) NOT NULL,
  `direccion` VARCHAR(100) NULL,
  `telefono` VARCHAR(10) NULL,
  PRIMARY KEY (`nombre`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `clubajedrez`.`hospedaje`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `clubajedrez`.`hospedaje` (
  `idhospedaje` INT NOT NULL AUTO_INCREMENT,
  `participante_numasociado` VARCHAR(6) NOT NULL,
  `hotel_nombre` VARCHAR(30) NOT NULL,
  `fechaentrada` VARCHAR(45) NULL,
  `fechasalida` VARCHAR(45) NULL,
  PRIMARY KEY (`idhospedaje`, `participante_numasociado`, `hotel_nombre`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `clubajedrez`.`sala`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `clubajedrez`.`sala` (
  `idsala` INT NOT NULL AUTO_INCREMENT,
  `capacidad` INT NULL,
  `medios` VARCHAR(100) NULL,
  `hotel_nombre` VARCHAR(30) NOT NULL,
  PRIMARY KEY (`idsala`, `hotel_nombre`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `clubajedrez`.`jornada`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `clubajedrez`.`jornada` (
  `idjornada` INT NOT NULL AUTO_INCREMENT,
  `perido` VARCHAR(45) NULL,
  `año` VARCHAR(45) NULL,
  PRIMARY KEY (`idjornada`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `clubajedrez`.`partidas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `clubajedrez`.`partidas` (
  `codp` VARCHAR(5) NOT NULL,
  `arbitro_idarbitro` INT NOT NULL,
  `jornada_idjornada` INT NOT NULL,
  `sala_idsala` INT NOT NULL,
  `entradas` INT NULL,
  PRIMARY KEY (`codp`, `arbitro_idarbitro`, `jornada_idjornada`, `sala_idsala`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `clubajedrez`.`color`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `clubajedrez`.`color` (
  `idcolor` INT NOT NULL AUTO_INCREMENT,
  `jugador_idjugador` INT NOT NULL,
  `color` VARCHAR(10) NULL,
  `partidas_codp` VARCHAR(5) NOT NULL,
  PRIMARY KEY (`idcolor`, `jugador_idjugador`, `partidas_codp`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `clubajedrez`.`movimientos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `clubajedrez`.`movimientos` (
  `partidas_codp` VARCHAR(5) NOT NULL,
  `idmovimiento` INT NOT NULL,
  `movimiento` VARCHAR(100) NULL,
  `comentario` VARCHAR(100) NULL,
  PRIMARY KEY (`partidas_codp`, `idmovimiento`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
